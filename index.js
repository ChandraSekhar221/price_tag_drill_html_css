let priceEl1 = document.getElementById('price1')
let priceEl2 = document.getElementById('price2')
let priceEl3 = document.getElementById('price3')

function ChangePrice() {
  if (priceEl1.innerText == 199.99) {
    priceEl1.innerText = 19.99
    priceEl2.innerText = 24.99
    priceEl3.innerText = 39.99
  } else {
    priceEl1.innerText = 199.99
    priceEl2.innerText = 249.99
    priceEl3.innerText = 399.99
  }
}
